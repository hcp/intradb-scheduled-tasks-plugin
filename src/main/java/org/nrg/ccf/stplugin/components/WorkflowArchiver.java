package org.nrg.ccf.stplugin.components;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.nrg.framework.task.XnatTask;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.xnat.task.AbstractXnatTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@XnatTask(taskId = "WorkflowArchiver", description = "Workflow Archiver Task", 
	defaultExecutionResolver = "SingleNodeExecutionResolver", executionResolverConfigurable = false)
@Slf4j
@Component
public class WorkflowArchiver extends AbstractXnatTask implements Runnable {
	
	private JdbcTemplate _jdbcTemplate;
	public final String _workflowArchiveQueryTemplateLoc = "/intradb-scheduled-tasks-plugin/WorkflowArchiveSql.vm";
    Template _workflowArchiveQueryTemplate = null;
    String _workflowArchiveQuery = null;
    
	@Autowired
	public WorkflowArchiver(XnatTaskService taskService, JdbcTemplate jdbcTemplate) {
		super(taskService);
		_jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public void run() {
		if (!shouldRunTask()) {
			log.info("Workflow archiver is not configured to run on this node.  Skipping.");
			return;
		}
		initializeQuery();
		if (_workflowArchiveQuery == null) { 
			log.error("Workflow Archiver Task: ERROR - could not return workflow archive query");
		}
		log.warn("Workflow Archiver Task: Run workflow archive query: \n" + _workflowArchiveQuery);
		_jdbcTemplate.execute(_workflowArchiveQuery);
	}
	
	private void initializeQuery() {
		try {
			if (_workflowArchiveQuery == null) { 
				final Template _workflowArchiveQueryTemplate =Velocity.getTemplate("/intradb-scheduled-tasks-plugin/WorkflowArchiveSql.vm");
				final StringWriter writer = new StringWriter();
				_workflowArchiveQueryTemplate.merge(new VelocityContext(), writer);
				_workflowArchiveQuery = writer.toString();
				writer.close();
			}
		} catch (IOException e) {
			log.warn("WARNING:  Could not build workflow archive cleanup task query", e);
		}
	}
	
}
