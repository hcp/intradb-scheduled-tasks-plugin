package org.nrg.ccf.stplugin.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "intradbScheduledTasksPlugin",
			name = "Intradb Scheduled Tasks Plugin"
		)
@ComponentScan({ 
		"org.nrg.ccf.stplugin.conf",
		"org.nrg.ccf.stplugin.components",
		"org.nrg.ccf.stplugin.scheduler"
	})
public class IntradbScheduledTasksPlugin {
	
	public static Logger logger = Logger.getLogger(IntradbScheduledTasksPlugin.class);

	public IntradbScheduledTasksPlugin() {
		logger.info("Configuring the IntraDB scheduled tasks plugin.");
	}
	
}
