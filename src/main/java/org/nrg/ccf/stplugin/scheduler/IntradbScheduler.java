package org.nrg.ccf.stplugin.scheduler;

import org.nrg.ccf.stplugin.components.WorkflowArchiver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.config.TriggerTask;
import org.springframework.scheduling.support.CronTrigger;

@Configuration
@EnableScheduling
public class IntradbScheduler {
	
    @Bean
    public TriggerTask workflowArchiveTrigger(WorkflowArchiver workflowArchiveService) {
        return new TriggerTask(workflowArchiveService, new CronTrigger("0 0 3 * * *"));
    }

}